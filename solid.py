# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

3D vector implementation

Implements
==========

 - Solid

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

from facet import Facet


class Solid(object):
    """
    """
    def __init__(self, name="Onshape"):
        """
        """
        self._name = name

        self._facets = []

    def __str__(self):
        s = "solid %s\n" % self._name
        for facet in self._facets:
            s += "%s\n" % facet
        s += "endsolid %s\n" % self._name

        return s

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def facets(self):
        return self._facets

    def addFacet(self, facet):
        """
        """
        self._facets.append(facet)


if __name__ == '__main__':
    import unittest


    class SolidTestCase(unittest.TestCase):

        def setUp(self):
            pass

        def tearDown(self):
            pass


    unittest.main()
