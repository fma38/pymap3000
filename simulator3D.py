# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

5 axis 3D printing simulator

Implements
==========

 - Simulator3D

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

import visual as vpython

from tools.stl import stlToFaces


class Head():
    """
    """
    def __init__(self):
        """
        """
        self._build3D()

    def _build3D(self):
        """
        """
        self._head = vpython.cone(axis=vpython.vector(0, -1, 0),
                                  pos=vpython.vector(0, 25, 0),
                                  radius=10,
                                  length=25)

    def translate(self, x, y):
        """
        """
        self._head.pos.x = x
        self._head.pos.z = -y


#class Nozzle():
    #"""
    #"""
    #def __init__(self):
        #"""
        #"""
        #self._build3D()

    #def _build3D(self):
        #"""
        #"""
        #self._nozzle = vpython.sphere(pos=vpython.vector(0, 0, 0),
                                      #radius=0.1,
                                      #make_trail=True)

    #def translate(self, x=None, y=None, z=None):
        #"""
        #"""
        #if x is not None:
            #self._nozzle.pos.x = x
        #if y is not None:
            #self._nozzle.pos.z = -y
        ##if z is not None:
            ##self._nozzle.pos.y = -z


class Bed():
    """
    """
    def __init__(self):
        """
        """
        self._a = 0.
        self._b = 0.

        self._build3D()

    def _build3D(self):
        """
        """
        self._bedFrame = vpython.frame(axis=vpython.vector(1, 0, 0))

        self._bed = stlToFaces("parts/bed.stl")
        self._bed.frame.frame = self._bedFrame
        self._bed.color = vpython.color.yellow
        #self._bed.frame.pos = vpython.vector(0, -55, 0)
        self._bed.frame.rotate(angle=math.radians(-90))

        #self._pt1 = vpython.ball(pos=vpython.vector(200, 0, 0))
        #self._pt2 = vpython.ball(pos=vpython.vector(0, 0, -200))

    def rotate(self, a, b):
        """
        """
        a = vpython.radians(a)
        b = -vpython.radians(b)

        # Revert previous rotation
        self._bedFrame.rotate(angle=-self._a, axis=vpython.vector(1, 0, 0))
        self._bed.frame.rotate(angle=-self._b, axis=vpython.vector(0, 0, 1))

        # New rotation
        self._bedFrame.rotate(angle=a, axis=vpython.vector(1, 0, 0))
        self._bed.frame.rotate(angle=b, axis=vpython.vector(0, 0, 1))

        self._a = a
        self._b = b

    def translate(self, z):
        """
        """
        self._bedFrame.pos.y = -z


def main():
    head = Head()
    bed = Bed()
    bed._bed.frame.pos = vpython.vector(0, -55, 0)

    #vpython.scene.append_to_caption('\n\n')
    #vpython.scene.append_to_caption('A: ')
    #slA = vpython.slider(bind=bed.rotateA, min=-35., max=35., value=0.)
    #vpython.scene.append_to_caption("\n\n")
    #vpython.scene.append_to_caption('B: ')
    #slB = vpython.slider(bind=bed.rotateB, min=-35., max=35., value=0.)
    #vpython.scene.append_to_caption("\n\n")
    #vpython.scene.append_to_caption('Z: ')
    #slZ = vpython.slider(bind=bed.translate, min=-300., max=0., value=0.)
    #vpython.scene.append_to_caption("\n\n")

    vpython.rate(25)


if __name__ == "__main__":
    main()
