# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Mapping/unmapping

Implements
==========

 - mapToPlane
 - unmapToSphere

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

from common import config
from common.logger import logger
from common.geometry import rotationMatrix, extractEulerAnglesXY
from vertex import Vertex
from vector import Vector


def mapToPlane(vertex):
    """ Map to plane
    """

    # Compute distance from center of sphere O to vertex A
    vector = Vector(vertex) - Vector([0, 0, config.BED_SPHERE_ORIGIN_Z])
    L = vector.length

    # Compte horizontal distance from Z axis to vertex
    Lu = math.sqrt(vertex.x ** 2 + vertex.y ** 2)

    # Compute angle between Z axis and PA
    alpha = math.atan2(Lu, vertex.z - config.BED_SPHERE_ORIGIN_Z)
    #alpha = math.asin(Lu / L)

    # Compute angle between X axis and PA in XY plane
    beta = math.atan2(vertex.y, vertex.x)

    # The mapping to plane is done by using the arc distance of vertex A as radius of mapped vertex A1
    u = L * alpha
    x = u * math.cos(beta)
    y = u * math.sin(beta)
    z = L - config.BED_RADIUS_INT

    return Vertex([x, y, z])


def unmapToSphere(vector, fiveAxis=True):
    """ Map back to sphere

    With bed rotation (A/B kinematic)
    """

    # Compute distance from center of sphere O to unmapped vertex A
    L = vector.z + config.BED_RADIUS_INT

    # Compute angle between Z axis and PA
    alpha = math.sqrt(vector.x ** 2 + vector.y ** 2) / L

    # Compute angle between X axis and PA in XY plane
    beta = math.atan2(vector.y, vector.x)
    logger.debug("L=%.2f, alpha=%.1f, beta=%.1f)" % (L, math.degrees(alpha), math.degrees(beta)))

    # Compute horizontal distance from Z axis to unmapped vertex A,
    # and vertical distance from center of sphere O to unmapped vertex A
    Lu = L * math.sin(alpha)
    Lz = L * math.cos(alpha)

    # Unmapped vertex A coords
    x = Lu * math.cos(beta)
    y = Lu * math.sin(beta)
    z = Lz + config.BED_SPHERE_ORIGIN_Z
    if z < config.LAYER_HIGHT:
        z = config.LAYER_HIGHT
    A = Vector([x, y, z])

    # Compute bed rotation
    axis = vector.cross(Vector([0, 0, 1]))  # normal to Z axis and current point vector
    R = rotationMatrix(axis, alpha)
    a, b = extractEulerAnglesXY(R)

    # Rotated vertex A2 coords
    import numpy
    A = numpy.matrix([[A.x],
                      [A.y],
                      [A.z]])
    A2 = R * A

    A = Vector([float(A[0]), float(A[1]), float(A[2])])
    A2 = Vector([float(A2[0]), float(A2[1]), float(A2[2])])

    logger.debug("x1=%.2f, y1=%.2f, z1=%.2f -> x=%.2f, y=%.2f, z=%.2f -> x2=%.2f, y2=%.2f, z2=%.2f" %
                 (vector.x, vector.y, vector.z, A.x, A.y, A.z, A2.x, A2.y, A2.z)),

    ## Test: compute coords of the 2 pushing points
    #pt1 = numpy.matrix([[200],
                        #[0],
                        #[0]])
    #pt2 = numpy.matrix([[0],
                        #[200],
                        #[0]])

    #pt1 = R * pt1
    #pt2 = R * pt2

    #pt1 = Vector([float(pt1[0]), float(pt1[1]), float(pt1[2]))
    #pt2 = Vector([float(pt2[0]), float(pt2[1]), float(pt2[2]))

    if fiveAxis:
        return A2, a, b  #, pt1, pt2
    else:
        return A, 0., 0.
