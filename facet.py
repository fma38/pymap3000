# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

3D vector implementation

Implements
==========

 - Facet

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

from vector import Vector


class Facet(object):
    """
    """
    def __init__(self, vertices, normal=None):
        """
        """
        if len(vertices) != 3:
            raise ValueError("A facet must have 3 vertices")

        self._vertices = vertices

        if normal is not None:
            self._normal = normal
        else:
            vect1 = Vector(vertices[1] - vertices[0])
            vect2 = Vector(vertices[2] - vertices[0])
            self._normal = vect1.cross(vect2)
            self._normal /= self._normal.length

    @property
    def normal(self):
        return self._normal

    @property
    def vertices(self):
        return self._vertices

    def __str__(self):
        s = "  facet normal %s\n" % self._normal
        s += "    outer loop\n"
        for vertex in self._vertices:
            s += "      vertex %s\n" % vertex
        s += "    endloop\n"
        s += "  endfacet"

        return s


if __name__ == '__main__':
    import unittest

    from vertex import Vertex


    class FacetTestCase(unittest.TestCase):

        def setUp(self):
            vertex0 = Vertex([33.6331, -0.859882, 7.94025])
            vertex1 = Vertex([33.7304, -1.27721, 7.87598])
            vertex2 = Vertex([36.9496, -1.35799, 5.8738])
            vertices0 = [vertex0, vertex1, vertex2]
            normal0 = Vector([0.527976, -0.00773871, 0.849224])
            self.facet0 = Facet(vertices0, normal0)

            vertex3 = Vertex([35.23611, -0.90087, 1.99998])
            vertex4 = Vertex([35.35011, -1.33854, 2.00000])
            vertex5 = Vertex([39.14122, -1.43854, 1.99999])
            vertices1 = [vertex3, vertex4, vertex5]
            self.facet1 = Facet(vertices1)

        def tearDown(self):
            pass

        def testNormal(self):
            self.assertAlmostEqual(self.facet1.normal.x, 0.00000, places=5)
            self.assertAlmostEqual(self.facet1.normal.y, 0.00005, places=5)
            self.assertAlmostEqual(self.facet1.normal.z, 1.00000, places=5)


    unittest.main()
