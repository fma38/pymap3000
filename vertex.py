# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

3D vector implementation

Implements
==========

 - Vertex

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division


class Vertex():
    """
    """
    def __init__(self, coords):
        """
        """
        if len(coords) != 3:
            raise ValueError("Vertex is a 3D vertex")

        self._coords = coords

    def __str__(self):
        return "%.5f %.5f %.5f" % (self[0], self[1], self[2])

    def __getitem__(self, key):
       return self._coords[key]

    def __setitem__(self, key, value):
       self._coords[key] = value

    def __add__(self, other):
        if isinstance(other, Vertex):
            res = [a+b for a, b in zip(self._coords, other._coords)]
            return Vertex(res)

        elif type(other) in (int, float):
            res = [a+other for a in self._coords]
            return Vertex(res)

        else:
            raise ValueError("Can only add another Vertex or int/float")

    def __sub__(self, other):
        if isinstance(other, Vertex):
            res = [a-b for a, b in zip(self._coords, other._coords)]
            return Vertex(res)

        elif type(other) in (int, float):
            return self._coords - other

        else:
            raise ValueError("Can only substract another Vertex or int/float")

    def __len__(self):
        return len(self._coords)

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def z(self):
        return self[2]


if __name__ == '__main__':
    import unittest


    class VertexTestCase(unittest.TestCase):

        def setUp(self):
            pass

        def tearDown(self):
            pass


    unittest.main()
