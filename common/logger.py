# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Logginf.

Implements
==========

Documentation
=============

Usage
=====

Bugs
====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from common import config


class Logger():
    """
    """
    def __init__(self):
        """
        """

    def debug(self, mesg):
        """
        """
        if config.DEBUG_LEVEL <= config.DEBUG:
            print("; DEBUG::%s" % mesg)

    def warning(self, mesg):
        """
        """
        if config.DEBUG_LEVEL <= config.WARNING:
            print("; WARNING::%s" % mesg)

    def info(self, mesg):
        """
        """
        if config.DEBUG_LEVEL <= config.INFO:
            print("; INFO::%s" % mesg)


logger = Logger()
