# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Euler rotations handling.

Implements
==========

 - rotationMatrix
 - extractEulerAnglesXY

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

import numpy

from vector import Vector


def rotationMatrix(axis, angle):
    """ Compute the rotation matrix.

    See: http://fr.wikipedia.org/wiki/Matrice_de_rotation#Axe_de_rotation
    """
    if axis.length != 0:
        u = axis / axis.length  # normalized length vector
        P = numpy.array([[u.x * u.x, u.x * u.y, u.x * u.z],
                         [u.x * u.y, u.y * u.y, u.y * u.z],
                         [u.x * u.z, u.y * u.z, u.z * u.z]])
        I = numpy.identity(3)
        Q = numpy.array([[   0, -u.z,  u.y],
                         [ u.z,    0, -u.x],
                         [-u.y,  u.x,    0]])
        rotation = P + (I - P) * math.cos(angle) + Q * math.sin(angle)

    else:
        rotation = numpy.identity(3)

    return rotation


#def rotationAngles(R):
    #""" Get the rotations angle of the rotation matrix.

    #Is it correct?
    #"""
    #x = math.atan2(R[2][1], R[2][2])
    #y = math.atan2(-R[2][0], math.sqrt(R[0][0] ** 2 + R[1][0] ** 2))
    #z = math.atan2(R[1][0], R[0][0])

    #return x, y, z


#def rotateVector(V, x=None, y=None, z=None):
    #""" Rotate a vector in space.
    #"""
    #if x is not None:
        #xRot = rotationMatrix(Vector([1, 0, 0]), x)
        #V = Vector(matrixmultiply(xRot, V))

    #if y is not None:
        #yRot = rotationMatrix(Vector([0, 1, 0]), y)
        #V = Vector(matrixmultiply(yRot, V))

    #if z is not None:
        #zRot = rotationMatrix(Vector([0, 0, 1]), z)
        #V = Vector(matrixmultiply(zRot, V))

    #return V


#def computeEulerRotationMatrixXYZ(phi, theta, psi):
    #"""
    #"""
    #X = numpy.array([[1, 0,              0            ],
                     #[0, math.cos(phi), -math.sin(phi)],
                     #[0, math.sin(phi),  math.cos(phi)]])

    #Y = numpy.array([[math.cos(theta),  0, math.sin(theta)],
                     #[0,                1, 0         ],
                     #[-math.sin(theta), 0, math.cos(theta)]])

    #Z = numpy.array([[math.cos(psi), -math.sin(psi), 0],
                     #[math.sin(psi),  math.cos(psi), 0],
                     #[0,              0,             1]])

    #R = matrixmultiply(Z, Y)
    #R = matrixmultiply(R, X)

    #return R


#def computeEulerRotationMatrixZXY(phi, theta, psi):
    #"""
    #"""
    #X = numpy.array([[1, 0,         0       ],
                     #[0, math.cos(phi), -math.sin(phi)],
                     #[0, math.sin(phi),  math.cos(phi)]])

    #Y = numpy.array([[math.cos(theta),  0, math.sin(theta)],
                     #[0,           1, 0         ],
                     #[-math.sin(theta), 0, math.cos(theta)]])

    #Z = numpy.array([[math.cos(psi), -math.sin(psi), 0],
                     #[math.sin(psi),  math.cos(psi), 0],
                     #[0,         0,        1]])

    #R = matrixmultiply(Y, X)
    #R = matrixmultiply(R, Z)

    #return R


#def computeEulerRotationMatrixXY(phi, theta):
    #"""
    #"""
    #X = numpy.array([[1, 0,         0       ],
                     #[0, math.cos(phi), -math.sin(phi)],
                     #[0, math.sin(phi),  math.cos(phi)]])

    #Y = numpy.array([[math.cos(theta),  0, math.sin(theta)],
                     #[0,           1, 0         ],
                     #[-math.sin(theta), 0, math.cos(theta)]])

    #R = matrixmultiply(Y, X)

    #return R


#def extractEulerAnglesXYZ(M):
    #""" Extract Euler angles from rotation matrix.
    #"""

    ## Rotation XYZ
    #phi = math.atan2(M[2][1], M[2][2])
    #theta = math.atan2(-M[2][0], math.sqrt(M[0][0] ** 2 + M[1][0] ** 2))
    #psi = math.atan2(M[1][0], M[0][0])

    #return phi, theta, psi


#def extractEulerAnglesZXY(M):
    #""" Extract Euler angles from rotation matrix.
    #"""

    ## Rotation ZXY
    #phi = math.atan2(-M[1][2], math.sqrt(M[1][0] ** 2 + M[1][1] ** 2))
    #theta = math.atan2(M[0][2], M[2][2])
    #psi = math.atan2(M[1][0], M[1][1])

    #return phi, theta, psi


def extractEulerAnglesXY(M):
    """ Extract Euler angles from rotation matrix.
    """

    # Rotation XY
    phi = math.atan2(-M[1][2], M[1][1])
    theta = math.atan2(-M[2][0], M[0][0])

    return phi, theta
