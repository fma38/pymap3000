# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Configuration.

Implements
==========

Documentation
=============

Usage
=====

Bugs
====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

TRACE = 1
DEBUG = 2
INFO = 3
WARNING = 4
ERROR = 5
CRITICAL = 6
DEBUG_LEVEL = WARNING
#DEBUG_LEVEL = DEBUG

# Bed config
BED_CAN_TILT = True
BED_RADIUS_INT = 253.
BED_SHIFT_Z = 55.
BED_SPHERE_ORIGIN_Z = BED_SHIFT_Z - BED_RADIUS_INT
BED_CENTER_X = 0.  #160.
BED_CENTER_Y = 0.  #160.

# Part config
#PART_SHIFT_Z = 0.

# Printing config
LAYER_HIGHT = 0.2

# map params

# unmap params
MAX_MOVE_LENGTH = 1.
