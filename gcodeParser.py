# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

G-Code parser

Implements
==========

 - GCodeParser

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import sys
import collections

from common.logger import logger


class GCodeParser(object):
    """
    """
    def __init__(self, inputFile, outputFile):
        """
        """
        self._inputFile = inputFile
        self._outputFile = outputFile

        self._numLine = 0
        self._line = ""

    def _parseLine(self):
        """
        """

        # Strip comments
        bits = self._line.split(';', 1)
        try:
            comment = bits[1]
        except IndexError:
            comment = ""

        # Extract and clean command
        command = bits[0].strip()

        # TODO strip logical line number and checksum

        # Code is first word, then args
        comm = command.split(None, 1)
        code = comm[0] if (len(comm) > 0) else None
        args = comm[1] if (len(comm) > 1) else None

        if code:
            if hasattr(self, "_parse%s" % code):
                logger.info("processing '%s'" % self._line)
                getattr(self, "_parse%s" % code)(args)
                logger.info("end processing '%s'" % self._line)
            else:
                self._outputFile.write("%s\n" % self._line)
        else:
            self._outputFile.write("%s\n" % self._line)

    def _parseArgs(self, args):
        """
        """
        argsDict = collections.OrderedDict()
        bits = args.split()
        for bit in bits:
            letter = bit[0]
            coord = float(bit[1:])
            argsDict[letter] = coord

        return argsDict

    def parse(self):
        """
        """
        self._numLine = 0

        # Parse lines
        for line in self._inputFile:
            self._line = line.rstrip()
            self._parseLine()
            self._numLine += 1
