#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Map a spherical STL part to a plane.

Implements
==========

Documentation
=============

Usage
=====

Bugs
====

 - re-compute speed when adding Z move
 - which path to take on the sphere? -> In vertical plane

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import sys
import argparse


def _map(inputFile, outputFile):
    """
    """

    from stl import STL

    stl = STL(inputFile, outputFile)
    stl.parse()


def _unmap(inputFile, outputFile):
    """
    """
    from gcode import GCode

    gcode = GCode(inputFile, outputFile)
    gcode.parse()


def _simul(inputFile, outputFile):
    """
    """
    from gcodeSimulator import GCodeSimulator

    gcode = GCodeSimulator(inputFile, outputFile)
    gcode.parse()


def main():
    """
    """

    # Common options
    parser = argparse.ArgumentParser(prog="pymap3000.py",
                                     description="This tool is used to map/unmap a portion of sphere for 5-axis printing.",
                                     epilog="Under developement...")

    # Common sub-parsers options
    parentSubParser = argparse.ArgumentParser(add_help=False)
    parentSubParser.add_argument("inputFile", nargs='?', type=argparse.FileType('r'),
                                 default=sys.stdin, metavar="INPUT_FILE",
                                 help="input file")
    parentSubParser.add_argument("outputFile", nargs='?', type=argparse.FileType('w'),
                                 default=sys.stdout, metavar="OUTPUT_FILE",
                                 help="output file")

    # Create sub-parsers
    subparsers = parser.add_subparsers(title="subcommands", description="valid subcommands",
                                       help="sub-command help")

    # Map to sphere parser
    parserMap = subparsers.add_parser("map",
                                      parents=[parentSubParser],
                                      help="map a spherical STL file to plane")
    parserMap.set_defaults(func=_map)

    # Unmap parser
    parserUnmap = subparsers.add_parser("unmap",
                                        parents=[parentSubParser],
                                        help="unmap a planar G-Code file back to sphere")
    parserUnmap.set_defaults(func=_unmap)

    # Simul parser
    parserUnmap = subparsers.add_parser("simul",
                                        parents=[parentSubParser],
                                        help="simulate 5 axis G-Code")
    parserUnmap.set_defaults(func=_simul)

    # Parse
    args = parser.parse_args()
    #print(vars(args))

    # Python3 does not automatically handle empty args
    if 'func' not in vars(args):
        args = parser.parse_args(["--help"])

    args.func(args.inputFile, args.outputFile)


if __name__ == "__main__":
    main()
