# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

G-Code simulator

Implements
==========

 - GCodeSimulator

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import time

from common import config
from common.logger import logger
from gcodeParser import GCodeParser
from simulator3D import Head, Bed  #, Nozzle


class GCodeSimulator(GCodeParser):
    """
    """
    def __init__(self, inputFile, outputFile):
        """
        """
        super(GCodeSimulator, self).__init__(inputFile, outputFile)

        self._head = Head()
        #self._nozzle = Nozzle()
        self._bed = Bed()

    def _parseG0(self, args):
        """ G0: Rapid move

        Treated as G1.
        """
        self.parseG1(args, code="G0")

    def _parseG1(self, args, code="G1"):
        """ G1: Controlled move
        """
        args = self._parseArgs(args)
        #logger.debug("args=%s" % args)
        #print(args)

        #line = "%s " % code
        #for key, value in args.items():
            #line += "%s=%s " % (key, value)
        #print(line)

        # Decode supported moves
        if len(args) == 1:
            if 'F' in args:
                moveType = 'F'
            elif 'Z' in args:
                moveType = 'Z'

        elif len(args) == 2:
            if 'X' in args and 'Y' in args:
                moveType = 'XY'

        elif len(args) == 5:
            moveType = 'XYZAB'

        elif len(args) == 6:
            moveType = 'XYZABE'

        else:
            raise Exception("Unsupported %s move" % code)

        # Handle supported moves
        if moveType == 'F':
            pass

        if moveType == 'Z':
            self._bed.translate(args['Z'])
            #self._nozzle.translate(args['Z'])
            time.sleep(0.05)

        elif moveType in ('XYZAB', 'XYZABE'):
            self._head.translate(args['X'], args['Y'])
            #self._nozzle.translate(args['X'], args['Y'], args['Z'])  #+config.PART_SHIFT_Z),
            self._bed.translate(args['Z'])  #+config.PART_SHIFT_Z)
            self._bed.rotate(args['A'], args['B'])
            time.sleep(0.002)

    def _parseG91(self, args):
        """ G91: Set to Relative Positioning
        """
        self._error("Relative coordinates not supported")

    def _parseG92(self, args):
        """ G92: Set current Position
        """
        self._error("G92 not supported")
