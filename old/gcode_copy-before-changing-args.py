# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

G-Code parser

Implements
==========

 - GcodeParser

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from vector import Vector


class GcodeParser():
    """
    """
    def __init__(self, transform=None):
        """
        """
        self._transform = transform

        self._numLine = 0
        self._line = ""

        self._previousArgs = {
            'X': 0.,
            'Y': 0.,
            'Z': 0.,
            'F': 0
        }
        self._moveVectorLength = 0
        self._moveVectorUnmappedLength = 0.

    def _error(self, msg):
        """
        """
        raise Exception("[ERROR] Line %d: %s (Text:'%s')" % (self._numLine, msg, self._line))

    def _parseLine(self):
        """
        """

        # Strip comments
        bits = self._line.split(';', 1)
        try:
            comment = bits[1]
        except IndexError:
            comment = ""

        # Extract and clean command
        command = bits[0].strip()

        # TODO strip logical line number and checksum

        # Code is first word, then args
        comm = command.split(None, 1)
        code = comm[0] if (len(comm) > 0) else None
        args = comm[1] if (len(comm) > 1) else None

        if code:
            if hasattr(self, "_parse%s" % code):
                print "; process '%s'" % self._line
                getattr(self, "_parse%s" % code)(args)
                print "; end process"
            else:
                print self._line
        else:
            print self._line

    def _parseArgs(self, args):
        """
        """
        argsDict = {}
        bits = args.split()
        for bit in bits:
            letter = bit[0]
            coord = float(bit[1:])
            argsDict[letter] = coord

        return argsDict

    def _parseG0(self, args):
        """ G0: Rapid move

        Treated as G1.
        """
        self.parseG1(args, code="G0")

    def _parseG1(self, args, code="G1"):
        """ G1: Controlled move

        Implemented for Slic3R / Prusaslicer; may need some adjustements for other slicers.
        Only support absolute coordinates and relative extrusion distance.

        Valid moves are:
            - G1 F...
            - G1 Z... F...
            - G1 X... Y...
            - G1 X... Y... E...
            - G1 X... Y... F...
        """
        args = self._parseArgs(args)
        newArgs = dict(self._previousArgs)  # local copy
        newArgs.update(args)
        #print newArgs

        # Compute move vector
        currentPosVector = Vector([self._previousArgs['X'], self._previousArgs['Y'], self._previousArgs['Z']])
        targetPosVector = Vector([newArgs['X'], newArgs['Y'], newArgs['Z']])
        moveVector = targetPosVector - currentPosVector

        # If segment is too long, split it in shorter ones, in order to move along the sphere
        # BUG: the path on the sphere should be in a vertical plane!
        if moveVector.length > MAX_MOVE_LENGTH:
            print "DEBUG::original length=%.2f" % moveVector.length
            self._moveVectorLength += moveVector.length
            nbSteps = int(moveVector.length / MAX_MOVE_LENGTH + 1)

            # Unmap segment ends on the sphere
            currentPosVectorUnmapped =  unmapToSphere(currentPosVector)
            targetPosVectorUnmapped = unmapToSphere(targetPosVector)

            # Compute vertical plane going through the segment ends

            # Iterate though steps
            for subStep in range(1, nbSteps+1):
                subMoveVector = currentPosVector + subStep / nbSteps * moveVector
                subMoveVectorUnmapped = unmapToSphere(subMoveVector)
                if subStep == 1:
                    newVector = newVector0 = subMoveVectorUnmapped
                    #print("; DEGUG::new vector=%s" % newVector)
                    #print("; DEGUG::new vector0=%s" % newVector0)
                    newLength = 0.
                else:
                    newVector = subMoveVectorUnmapped - newVector0
                    #print("; DEGUG::new vector=%s" % newVector)
                    newLength += newVector.length
                    #print("; DEGUG::new delta length=%.1f" % newVector.length)
                    newVector0 = subMoveVectorUnmapped
                    #print("; DEGUG::new vector0=%s" % newVector0)
                #line = "%s X%.2f Y%.2f Z%.2f A%.2f B%.2f F%0.1f" % (code,
                                                                     #subMoveVectorUnmapped.x, subMoveVectorUnmapped.y, subMoveVectorUnmapped.z,
                                                                     #0., 0.,
                                                                     #targetPosVector['F'])
                line = "%s X%.2f Y%.2f Z%.2f F%0.1f" % (code,
                                                        subMoveVectorUnmapped.x, subMoveVectorUnmapped.y, subMoveVectorUnmapped.z,
                                                        targetPosVector['F'])

                if 'E' in args.keys():
                    extrusion =  args['E'] / nbSteps
                    line += " E%.6f" % extrusion

                print(line)
                #print("; DEGUG::new length=%.2f" % newLength)

            print("DEBUG::new length=%.2f, " % newLength)
            self._moveVectorUnmappedLength += newLength

        else:
            subMoveVectorUnmapped = unmapToSphere(targetPosVector)
            #line = "%s X%.2f Y%.2f Z%.2f A%.2f B%.2f F%.1f" % (code,
                                                               #subMoveVectorUnmapped.x, subMoveVectorUnmapped.y, subMoveVectorUnmapped.z,
                                                               #0., 0.,
                                                               #targetPosVector['F'])
            line = "%s X%.2f Y%.2f Z%.2f F%.1f" % (code,
                                                   subMoveVectorUnmapped.x, subMoveVectorUnmapped.y, subMoveVectorUnmapped.z,
                                                   targetPosVector['F'])
            if 'E' in args.keys():
                line += " E%.5f" % args['E']

            print(line)

        for key in self._previousArgs.keys():
            try:
                self._previousArgs[key] = args[key]
            except KeyError:
                pass

    def _parseG91(self, args):
        """ G91: Set to Relative Positioning
        """
        self._error("Relative coordinates not supported")

    def _parseG92(self, args):
        """ G92: Set current Position
        """
        self._error("G92 not supported")

    def _parseM82(self, args):
        """ M82: Set extrusion to absolute distance
        """
        self._error("Absolute extrusion distance not supported")

    def setCallback(self, func):
        """
        """
        self._callback = func

    def parse(self, path):
        """
        """

        # Read the gcode file
        with open(path, 'r') as f:
            self._numLine = 0

            # Parse lines
            for line in f:
                self._line = line.rstrip()
                self._parseLine()
                self._numLine += 1
