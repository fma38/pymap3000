# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Map a spherical STL part to a plane.

Implements
==========

TODO

Documentation
=============

Ascii STL file:

solid name
    facet normal ni nj nk
        outer loop
            vertex v1x v1y v1z
            vertex v2x v2y v2z
            vertex v3x v3y v3z
        endloop
    endfacet
    ...
endsolid name

Binary STL file:

UINT8[80] – Header
UINT32 – Number of facets

    REAL32[3] – Normal vector
    REAL32[3] – Vertex 1
    REAL32[3] – Vertex 2
    REAL32[3] – Vertex 3
    UINT16 – Attribute byte count
    ...

Usage
=====

TODO

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import re
import sys
import math
import os.path

import numpy

#RADIUS_INT = 253.  # mm
#SPHERE_ORIGIN_Z = -206.484  # mm
RADIUS_INT = 65.  # mm
SPHERE_ORIGIN_Z = -50.  # mm


class Vector(list):
    """
    """
    def __str__(self):
        return "%.5f %.5f %.5f" % (self[0], self[1], self[2])


class Vertex(list):
    """
    """
    def __str__(self):
        return "%.5f %.5f %.5f" % (self[0], self[1], self[2])

    def mapToPlane(self, radiusInt, sphereOriginZ):
        """ Map to plane
        """
        L = math.sqrt(self[0] ** 2 + self[1] ** 2 + (self[2] - sphereOriginZ) ** 2)
        alpha = math.atan2(math.sqrt(self[0] ** 2 + self[1] ** 2), self[2] - sphereOriginZ)
        beta = math.atan2(self[1], self[0])

        x = alpha * L * math.cos(beta)
        y = alpha * L * math.sin(beta)
        z = L - radiusInt

        return Vertex([x, y, z])


class Facet(object):
    """
    """
    def __init__(self, normal, vertices):
        """
        """
        if len(vertices) != 3:
            raise ValueError("A facet must have 3 vertices")

        self._normal = normal
        self._vertices = vertices

    @property
    def normal(self):
        return self._normal

    @property
    def vertices(self):
        return self._vertices

    def __str__(self):
        s = "  facet normal %s\n" % self._normal
        s += "    outer loop\n"
        for vertex in self._vertices:
            s += "      vertex %s\n" % vertex
        s += "    endloop\n"
        s += "  endfacet"

        return s


class Solid(object):
    """
    """
    def __init__(self, name="Onshape"):
        """
        """
        self._name = name
        self._facets = []

    def __str__(self):
        s = "solid %s\n" % self._name
        for facet in self._facets:
            s += "%s\n" % facet
        s += "endsolid %s\n" % self._name

        return s

    @property
    def name(self):
        """
        """
        return self._name

    @property
    def facets(self):
        """
        """
        return self._facets

    def addFacet(self, facet):
        """
        """
        self._facets.append(facet)

    def map(self):
        """
        """
        newSolid = Solid("%s_mapped" % self._name)

        for i, facet in enumerate(self._facets):
            #print "%5d" % i,
            newVertices = []
            for vertex in facet.vertices:
                newVertex = vertex.mapToPlane(RADIUS_INT, SPHERE_ORIGIN_Z)
                newVertices.append(newVertex)

            # Compute new normal
            vector1 = [b-a for a, b in zip(newVertices[0], newVertices[1])]
            vector2 = [b-a for a, b in zip(newVertices[0], newVertices[2])]
            newNormal  = numpy.cross(vector1, vector2)
            newNormal /= numpy.sqrt(newNormal.dot(newNormal))

            newFacet = Facet(Vector(newNormal), newVertices)
            newSolid.addFacet(newFacet)

        return newSolid


def parseAscii(inputFile):
    """
    """
    inputFile.seek(0)
    inputStr = inputFile.read()

    # Iter over 'solid' (should be only one)
    for solidStr in re.findall(r"solid\s(.*?)endsolid", inputStr, re.S):
        solidName = re.match(r"^(.*)$", solidStr, re.M).group(0)
        print "Processing object '%s'..." % solidName

        solid = Solid(solidName)

        # Iter over 'facet normal'
        for facetNormalStr in re.findall(r"facet\snormal\s(.*?)endfacet", solidStr, re.S):
            coords = [float(coord) for coord in facetNormalStr.split('\n')[0].split()]
            normal = Vector(coords)

            # Iter over 'outer loop' (should be only one)
            for outerLoopStr in re.findall(r"outer\sloop(.*?)endloop", facetNormalStr, re.S):
                vertices = []

                # Iter over 'vertex'
                for vertexStr in re.findall(r"vertex\s(.*)$", outerLoopStr, re.M):
                    coords = [float(coord) for coord in vertexStr.split()]
                    vertex = Vertex(coords)
                    vertices.append(vertex)

            facet = Facet(normal, vertices)
            solid.addFacet(facet)

    return solid


def parseBinary(inputFile):
    """
    """

    # Skip header
    inputFile.seek(80)

    nbFacets = struct.unpack("<I", inputFile.read(4))[0]
    print "found %d facets" % nbFacets

    solid = Solid("Onshape")

    # Iterate over facets
    for i in range(nbFacets):

        # Get normal
        coords = struct.unpack("<fff", inputFile.read(3*4))
        normal = Vector(coords)

        vertices = []

        # Iterate over vertices
        for j in range(3):
            coords = struct.unpack("<fff", inputFile.read(3*4))
            vertex = Vertex(coords)
            vertices.append(vertex)

        facet = Facet(normal, vertices)

        solid.addFacet(facet)

        # Skip byte count
        inputFile.seek(2, 1)

    return solid


def main():
    """
    """
    inputFileName = sys.argv[1]
    inputFile = file(inputFileName)

    # Check if ascii or binary
    if inputFile.read(5) == "solid":
        print "ascii file"
        solid = parseAscii(inputFile)
    else:
        print "binary file"
        solid = parseBinary(inputFile)

    inputFile.close()

    outputFileName = "%s_mapped%s%s" % (os.path.splitext(inputFileName)[0], os.path.extsep, "stl")
    outputFile = file(outputFileName, "w")

    newSolid = solid.map()

    outputFile.write(str(newSolid))

    outputFile.close()

    print "%s saved" % outputFileName


if __name__ == "__main__":
    main()
