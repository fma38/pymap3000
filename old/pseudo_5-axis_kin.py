# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Transform planar coordinates to spherical coordinates for 5 axis 3D printer.
Also split moves in short segments to travel along the sphere without colliding

Implements
==========

Documentation
=============

Usage
=====

TODO
====

 - implement Euler rotations

Problem: - re-compute speed when adding Z move
         - which path to take on the sphere?

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import sys
import math

from vector import Vector

#RADIUS_INT = 253.  # mm
#SPHERE_ORIGIN_Z = -206.484  # mm
#BED_ROTATION_ORIGIN_Z =
RADIUS_INT = 65.  # mm
SPHERE_ORIGIN_Z = -50.  # mm
BED_ROTATION_ORIGIN_Z = 0.  # mm

MAX_MOVE_LENGTH = 1  # mm


def unMap(coords, radiusInt=RADIUS_INT, sphereOriginZ=SPHERE_ORIGIN_Z):
    """ Map back to sphere
    """

    # Common values
    L = coords['Z'] + RADIUS_INT
    #print "(L=%05.1f," % L,

    alpha = math.sqrt(coords['X'] ** 2 + coords['Y'] ** 2) / L
    #print "alpha=%05.1f," % math.degrees(alpha),

    beta = math.atan2(coords['Y'], coords['X'])
    #print "beta=%05.1f)" % math.degrees(beta),

    Lz = L * math.cos(alpha)
    Lu = L * math.sin(alpha)

    # Translations
    x = Lu * math.cos(beta)
    y = Lu * math.sin(beta)
    z = Lz + SPHERE_ORIGIN_Z  # + coords['Z']
    if z < 0.2:
        z = 0.2

    # Rotations
    # Note: these are not Euler rotations! This is for a real 6 axis kinematic, like a 6 axis delta printer.
    #       If using a tilting bed with stacked rotations, we need to implement Euler rotations
    U = Vector([0., 0., Lz])
    Va = Vector([x,  0., Lz])
    Ul = U.length
    a = math.acos(U.dot(Va) / (Ul * Va.length))
    Vb = Vector([0., y,  Lz])
    b = math.acos(U.dot(Vb) / (Ul * Vb.length))

    # Compute offsets due to rotation
    # TODO
    dx = 0.
    dy = 0.
    dz = 0.

    return {'X': x+dx, 'Y': y+dy, 'Z': z+dz, 'A': math.degrees(a), 'B': math.degrees(b)}


class GcodeConverter(object):
    """
    """
    def __init__(self):
        """
        """
        self._numLine = 0
        self._line = ""

        self._previousArgs = {
            'X': 0.,
            'Y': 0.,
            'Z': 0.,
            'F': 0
        }
        self._moveVectorLength = 0
        self._moveVectorUnmappedLength = 0.

    def parse(self, path):
        """
        """

        # Read the gcode file
        with open(path, 'r') as f:
            self._numLine = 0

            # Parse lines
            for line in f:
                self._line = line.rstrip()
                self._parseLine()
                self._numLine += 1

    def _parseLine(self):
        """
        """

        # Strip comments
        bits = self._line.split(';', 1)
        try:
            comment = bits[1]
        except IndexError:
            comment = ""

        # Extract and clean command
        command = bits[0].strip()

        # TODO strip logical line number and checksum

        # Code is first word, then args
        comm = command.split(None, 1)
        code = comm[0] if (len(comm) > 0) else None
        args = comm[1] if (len(comm) > 1) else None

        if code:
            if hasattr(self, "_parse%s" % code):
                print "; process '%s'" % self._line
                getattr(self, "_parse%s" % code)(args)
                print "; end process"
            else:
                print self._line
        else:
            print self._line

    def _parseArgs(self, args):
        """
        """
        argsDict = {}
        bits = args.split()
        for bit in bits:
            letter = bit[0]
            coord = float(bit[1:])
            argsDict[letter] = coord

        return argsDict

    def _parseG0(self, args):
        """ G0: Rapid move

        Treated as G1.
        """
        self.parseG1(args, code="G0")

    def _parseG1(self, args, code="G1"):
        """ G1: Controlled move

        Implemented for Slic3R / Prusaslicer; may need some adjustements for other slicers.
        Only support absolute coordinates and relative extrusion distance.

        Valid moves are:
            - G1 F...
            - G1 Z... F...
            - G1 X... Y...
            - G1 X... Y... E...
            - G1 X... Y... F...
        """
        args = self._parseArgs(args)
        newArgs = dict(self._previousArgs)  # local copy
        newArgs.update(args)
        #print newArgs

        # Compute move vector
        currentPosVector = Vector([self._previousArgs['X'],
                                        self._previousArgs['Y'],
                                        self._previousArgs['Z']])
        targetPosVector = Vector([newArgs['X'],
                                       newArgs['Y'],
                                       newArgs['Z']])
        moveVector = targetPosVector - currentPosVector
        moveVectorLength = moveVector.length

        # If segment is too long, split it in shorter ones, in order to move along the sphere
        # Important: the path on the sphere must be in a vertical plane
        if moveVectorLength > MAX_MOVE_LENGTH:
            print "DEBUG::original length=%.2f" % moveVectorLength
            self._moveVectorLength += moveVectorLength
            nbSteps = int(moveVectorLength / MAX_MOVE_LENGTH + 1)

            # Unmap segment extremities on the sphere
            currentPosVectorUnmapped =  unMap(dict(X=self._previousArgs['X'], Y=self._previousArgs['Y'], Z=self._previousArgs['Z']))
            targetPosVectorUnmapped = unMap(dict(X=newArgs['X'], Y=newArgs['Y'], Z=newArgs['Z']))

            # Compute vertical plane going through the segment extremities

            for subStep in range(1, nbSteps+1):
                subMoveVector = currentPosVector + subStep / nbSteps * moveVector
                subMoveVectorUnmapped = unMap(dict(X=subMoveVector[0], Y=subMoveVector[1], Z=subMoveVector[2]))
                if subStep == 1:
                    newVector = newVector0 = Vector([subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z']])
                    #print "DEGUG::new vector=%s" % newVector
                    #print "DEGUG::new vector0=%s" % newVector0
                    newLength = 0.
                else:
                    newVector = Vector([subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z']]) - newVector0
                    #print "DEGUG::new vector=%s" % newVector
                    newLength += newVector.length
                    #print "DEGUG::new delta length=%.1f" % newVector.length
                    newVector0 = Vector([subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z']])
                    #print "DEGUG::new vector0=%s" % newVector0
                #line = "%s X%.2f Y%.2f Z%.2f A%.2f B%.2f F%0.1f" % (code,
                                                                     #subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z'],
                                                                     #0., 0.,
                                                                     #targetPosVector['F'])
                line = "%s X%.2f Y%.2f Z%.2f F%0.1f" % (code,
                                                        subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z'],
                                                        targetPosVector['F'])

                if 'E' in args.keys():
                    extrusion =  args['E'] / nbSteps
                    line += " E%.6f" % extrusion

                print line
                #print "DEGUG::new length=%.2f" % newLength
            print "DEBUG::new length=%.2f, " % newLength
            self._moveVectorUnmappedLength += newLength

        else:
            subMoveVectorUnmapped = unMap(dict(X=targetPosVector['X'], Y=targetPosVector['Y'], Z=targetPosVector['Z']))
            #line = "%s X%.2f Y%.2f Z%.2f A%.2f B%.2f F%.1f" % (code,
                                                               #subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z'],
                                                               #0., 0.,
                                                               #targetPosVector['F'])
            line = "%s X%.2f Y%.2f Z%.2f F%.1f" % (code,
                                                   subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z'],
                                                   targetPosVector['F'])
            if 'E' in args.keys():
                line += " E%.5f" % args['E']

            print line

        for key in self._previousArgs.keys():
            try:
                self._previousArgs[key] = args[key]
            except KeyError:
                pass

    def _parseG91(self, args):
        """ G91: Set to Relative Positioning
        """
        self._error("Relative coordinates not supported")

    def _parseG92(self, args):
        """ G92: Set current Position
        """
        self._error("G92 not supported")

    def _parseM82(self, args):
        """ M82: Set extrusion to absolute distance
        """
        self._error("Absolute extrusion distance not supported")

    def _warn(self, msg):
        """
        """
        print "[WARN] Line %d: %s (Text:'%s')" % (self._numLine, msg, self._line)

    def _error(self, msg):
        """
        """
        raise Exception("[ERROR] Line %d: %s (Text:'%s')" % (self._numLine, msg, self._line))


def test():
    """
    """
    import pylab

    angle = pylab.np.linspace(-pylab.np.pi, pylab.np.pi, 100, endpoint=True)
    X1c = 24.67 * pylab.np.cos(angle)
    Y1c = 24.67 * pylab.np.sin(angle)
    X2c = 23.98 * pylab.np.cos(angle)
    Y2c = 23.98 * pylab.np.sin(angle)
    pylab.plot(X1c, Y1c)
    pylab.plot(X2c, Y2c)

    for angle in (-45, -35, -25, -15, 0, 15, 25, 35, 45):
        X1 = []
        Y1 = []
        X2 = []
        Y2 = []
        startAngle = math.radians(angle)
        endAngle = math.radians(90-angle)
        start = Vector([24.67*math.cos(startAngle), 24.67*math.sin(startAngle), 0.])
        end = Vector([24.67*math.cos(endAngle), 24.67*math.sin(endAngle), 0.])
        moveVector = end - start
        moveVectorLength = moveVector.length

        nbSteps = int(moveVectorLength / MAX_MOVE_LENGTH + 1)
        for subStep in range(0, nbSteps+1):
            subMoveVector = start + subStep / nbSteps * moveVector
            subMoveVectorUnmapped = unMap(dict(X=subMoveVector[0], Y=subMoveVector[1], Z=subMoveVector[2]))
            if subStep == 0:
                newVector = newVector0 = Vector([subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z']])
                #print "DEGUG::new vector=%s" % newVector
                #print "DEGUG::new vector0=%s" % newVector0
                newLength = 0.
            else:
                newVector = Vector([subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z']]) - newVector0
                #print "DEGUG::new vector=%s" % newVector
                newLength += newVector.length
                #print "DEGUG::new delta length=%.1f" % newVector.length
                newVector0 = Vector([subMoveVectorUnmapped['X'], subMoveVectorUnmapped['Y'], subMoveVectorUnmapped['Z']])
                #print "DEGUG::new vector0=%s" % newVector0
            #print "%.2f %.2f" % (subMoveVector[0], subMoveVector[1]), "%(X).2f %(Y).2f" % subMoveVectorUnmapped
            X1.append(subMoveVector[0])
            Y1.append(subMoveVector[1])
            X2.append(subMoveVectorUnmapped['X'])
            Y2.append(subMoveVectorUnmapped['Y'])

        pylab.plot(X1, Y1)
        pylab.plot(X2, Y2)

    ax = pylab.gca()
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['bottom'].set_position(('data',0))
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_position(('data',0))

    pylab.show()


def test2():
    """
    """
    allCoords = [
        {'X':   0.0,                            'Y':   0.0,                            'Z':   0.0},
        {'X':  24.67,                            'Y':   0.0,                            'Z':   0.2},
        {'X':   0.0,                            'Y':  24.67,                            'Z':   0.0},
        {'X':  24.67*math.cos(math.radians(45)), 'Y':  24.67*math.sin(math.radians(45)), 'Z':   0.0},
        {'X':  30.0*math.cos(math.radians(45)), 'Y':  30.0*math.sin(math.radians(45)), 'Z':   0.0},
    ]
    for coords in allCoords:
        print "X%(X)+06.1f Y%(Y)+06.1f Z%(Z)+06.1f" % coords,
        coords = unMap(coords)
        print "-> X%(X)+06.1f Y%(Y)+06.1f Z%(Z)+06.1f A%(A)+05.1f B%(B)+05.1f " % coords


def main():
    """
    """
    converter = GcodeConverter()
    converter.parse(sys.argv[1])
    print converter._moveVectorLength, converter._moveVectorUnmappedLength


if __name__ == "__main__":
    #main()
    #test()
    text2()
