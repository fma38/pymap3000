# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

3D vector implementation

Implements
==========

 - Vector

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import copy

import numpy


class Vector():
    """
    """
    def __init__(self, coords):
        """
        """
        if len(coords) != 3:
            raise ValueError("Vector is a 3D vector")

        self._coords = numpy.array(coords)

    def __str__(self):
        return "%.5f %.5f %.5f" % (self.x, self.y, self.z)

    def __getitem__(self, key):
       return self._coords[key]

    def __setitem__(self, key, value):
       self._coords[key] = value

    def __mul__(self, other):
        if type(other) == type(self):
            return self.dot(other)

        else:
            res = other * self._coords
            return Vector(res)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __div__(self, other):
        res = self._coords / other
        return Vector(res)

    def __truediv__(self, other):
        """ Python3 or with __future__.division
        """
        res = self._coords / other
        return Vector(res)

    def __add__(self, other):
        if isinstance(other, Vector):
            res =  self._coords + other._coords
            return Vector(res)

        else:
            res = self._coords + other
            return Vector(res)

    def __sub__(self, other):
        if isinstance(other, Vector):
            res = self._coords - other._coords
            return Vector(res)

        else:
            res = self._coords - other
            return Vector(res)

    def __iter__(self):
        return self._coords.__iter__()

    def __len__(self):
        return len(self._coords)

    def __cmp__(self, other):
        if isinstance(other, Vector):
            if numpy.alltrue(self._coords == other._coords):
                return 0
            else:
                return 1

        else:
            raise ValueError("Can only compare with another Vector")

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def z(self):
        return self[2]

    @property
    def length(self):
        return numpy.sqrt(self*self)

    def cross(self, other):
        """
        """
        if isinstance(other, Vector):
            res = numpy.cross(self._coords, other._coords)
            return Vector(res)

        else:
            raise ValueError("Can only cross with another Vector")

    def dot(self, other):
        """
        """
        if isinstance(other, Vector):
            return self._coords.dot(other._coords)

        else:
            raise ValueError("Can only multiply with another Vector")


if __name__ == '__main__':
    import unittest


    class VectorTestCase(unittest.TestCase):

        def setUp(self):
            self.v1 = Vector([1, 2, 3])
            self.v2 = Vector([4, 5, 6])

        def tearDown(self):
            pass

        def testCross(self):
            with self.assertRaises(ValueError):
                self.v1.cross([1, 2, 3])

        def testMul(self):
            self.assertEqual(self.v1*self.v2, 32)


    unittest.main()
