# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

G-Code unmapper

Implements
==========

 - GCode

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

from common import config
from common.logger import logger
from gcodeParser import GCodeParser
from vector import Vector
from transform import unmapToSphere


class GCode(GCodeParser):
    """
    """
    def __init__(self, inputFile, outputFile):
        """
        """
        super(GCode, self).__init__(inputFile, outputFile)

        self._currentPosVector = None

    def _parseG0(self, args):
        """ G0: Rapid move

        Treated as G1.
        """
        self.parseG1(args, code="G0")

    def _parseG1(self, args, code="G1"):
        """ G1: Controlled move

        Implemented for Slic3R / Prusaslicer (may need some adjustements for other slicers):
          - only support absolute coordinates and relative extrusion distance.
          - supported moves are:
            - G1 F...
            - G1 Z... F...
            - G1 X... Y...
            - G1 X... Y... E...
            - G1 X... Y... F...
        """
        args = self._parseArgs(args)
        #logger.debug("args=%s" % args)

        # Decode supported moves
        if len(args) == 1:
            if 'F' in args:
                moveType = 'F'

        elif len(args) == 2:
            if 'Z' in args and 'F' in args:
                moveType = 'ZF'

            elif 'X' in args and 'Y' in args:
                moveType = 'XY'

        elif len(args) == 3:
            if 'X' in args and 'Y' in args:
                if 'F' in args:
                    moveType = 'XYF'

                elif 'E' in args:
                    moveType = 'XYE'
        else:
            raise Exception("Unsupported %s move" % code)

        # Handle supported moves
        if 'F' in moveType:
            self._outputFile.write("%s F%d\n" % (code, args['F']))

        if moveType in ('ZF', 'XY', 'XYF', 'XYE'):

            # If self._currentPosVector is None, go to unmapToSphere([0, 0, 0]) coords on first move,
            #    going Z first, then X/Y
            if self._currentPosVector is None:
                if moveType == 'ZF':
                    targetPosVector = Vector([0, 0, args['Z']])
                else:
                    targetPosVector = Vector([0, 0, 0])

                unmappedTargetPosVector, a, b = unmapToSphere(targetPosVector, config.BED_CAN_TILT)

                self._outputFile.write("%s Z%.2f\n" % (code, unmappedTargetPosVector.z))
                self._outputFile.write("%s X%.2f Y%.2f\n" % (code,
                                                             unmappedTargetPosVector.x+config.BED_CENTER_X,
                                                             unmappedTargetPosVector.y+config.BED_CENTER_Y))

            else:

                # Compute move vector
                if moveType == 'ZF':
                    targetPosVector = Vector([self._currentPosVector.x, self._currentPosVector.y, args['Z']])
                else:
                    targetPosVector = Vector([args['X'], args['Y'], self._currentPosVector.z])

                moveVector = targetPosVector - self._currentPosVector
                #logger.debug("original path length=%.2f" % moveVector.length)

                unmappedCurrentPosVector, a, b = unmapToSphere(self._currentPosVector, config.BED_CAN_TILT)
                unmappedTargetPosVector, a, b = unmapToSphere(targetPosVector, config.BED_CAN_TILT)
                unmappedVector = unmappedTargetPosVector - unmappedCurrentPosVector

                if moveType == 'ZF' or moveVector.length <= config.MAX_MOVE_LENGTH:
                    #logger.debug("unmapped path length=%.2f" % unmappedVector.length)

                    if config.BED_CAN_TILT:
                        line = "%s X%.2f Y%.2f Z%.2f A%.2f B%.2f" % (code,
                                                                     unmappedTargetPosVector.x+config.BED_CENTER_X,
                                                                     unmappedTargetPosVector.y+config.BED_CENTER_Y,
                                                                     unmappedTargetPosVector.z,
                                                                     math.degrees(a),
                                                                     math.degrees(b))
                    else:
                        line = "%s X%.2f Y%.2f Z%.2f" % (code,
                                                         unmappedTargetPosVector.x+config.BED_CENTER_X,
                                                         unmappedTargetPosVector.y+config.BED_CENTER_Y,
                                                         unmappedTargetPosVector.z)

                    if 'E' in moveType:
                        line += " E%.5f" % args['E']

                    self._outputFile.write("%s\n" % line)

                # If segment is too long, cut it in shorter ones, in order to move along the sphere
                # TODO: the split should only occurs for umapped Z variation > MAX_Z_VARIATION
                # TODO: also 'split' A/B axes
                else:
                    nbSteps = int(moveVector.length / config.MAX_MOVE_LENGTH + 1)
                    #logger.debug("segment splitted in %d sub-segments" % nbSteps)
                    #logger.debug("original delta path length=%.3f" % (moveVector.length / nbSteps))

                    # Init sub-segment
                    unmappedVector = unmappedCurrentPosVector
                    ##logger.debug("unmappedVector=%s" % unmappedVector)
                    unmappedPathLength = 0.

                    # Iterate though sub-segments
                    for step in range(nbSteps):
                        logger.debug("segment %d/%d" % (step+1, nbSteps))
                        subMoveVector = self._currentPosVector + (step + 1) / nbSteps * moveVector
                        unmappedSubMoveVector, a, b = unmapToSphere(subMoveVector, config.BED_CAN_TILT)
                        unmappedDeltaVector = unmappedSubMoveVector - unmappedVector
                        ##logger.debug("unmappedDeltaVector=%s" % unmappedDeltaVector)
                        unmappedPathLength += unmappedDeltaVector.length
                        #logger.debug("unmapped delta path length=%.3f" % unmappedDeltaVector.length)
                        unmappedVector = unmappedSubMoveVector
                        ##logger.debug("unmappedVector=%s" % unmappedVector)

                        if config.BED_CAN_TILT:
                            line = "%s X%.2f Y%.2f Z%.2f A%.2f B%.2f" % (code,
                                                                         unmappedSubMoveVector.x+config.BED_CENTER_X,
                                                                         unmappedSubMoveVector.y+config.BED_CENTER_Y,
                                                                         unmappedSubMoveVector.z,
                                                                         math.degrees(a),
                                                                         math.degrees(b))
                        else:
                            line = "%s X%.2f Y%.2f Z%.2f" % (code,
                                                             unmappedSubMoveVector.x+config.BED_CENTER_X,
                                                             unmappedSubMoveVector.y+config.BED_CENTER_Y,
                                                             unmappedSubMoveVector.z)

                        if moveType == 'XYE':

                            # Correct extrusion
                            extrusion = args['E'] / moveVector.length * unmappedDeltaVector.length
                            line += " E%.6f" % extrusion
                            #line += " E%.6f" % (args['E'] / nbSteps)

                        self._outputFile.write("%s\n" % line)
                        ##logger.debug("unmapped path length=%.2f" % unmappedPathLength)

                    #logger.debug("unmapped path length=%.2f" % unmappedPathLength)
                    #logger.debug("error path length=%.1f%%" % ((unmappedPathLength - moveVector.length) / moveVector.length * 100))

            # Update current position
            self._currentPosVector = targetPosVector

    def _parseG91(self, args):
        """ G91: Set to Relative Positioning
        """
        raise ValueError("Relative coordinates not supported")

    def _parseG92(self, args):
        """ G92: Set current Position
        """
        raise ValueError("G92 not supported")

    def _parseM82(self, args):
        """ M82: Set extrusion to absolute distance
        """
        raise ValueError("Absolute extrusion distance not supported")
