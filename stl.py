# -*- coding: utf-8 -*-

"""

License
=======

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

3D vector implementation

Implements
==========

 - STL

Documentation
=============

Ascii STL file:

solid name
    facet normal ni nj nk
        outer loop
            vertex v1x v1y v1z
            vertex v2x v2y v2z
            vertex v3x v3y v3z
        endloop
    endfacet
    ...
endsolid name

Binary STL file:

UINT8[80] – Header
UINT32 – Number of facets

    REAL32[3] – Normal vector
    REAL32[3] – Vertex 1
    REAL32[3] – Vertex 2
    REAL32[3] – Vertex 3
    UINT16 – Attribute byte count
    ...

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2019 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import re
import struct
import copy

from vector import Vector
from vertex import Vertex
from solid import Solid
from facet import Facet
from transform import mapToPlane


class STL():
    """
    """
    def __init__(self, inputFile, outputFile):
        """
        """
        self._inputFile = inputFile
        self._outputFile = outputFile

        self._solid = Solid()

    @property
    def solid(self):
        return copy.deepcopy(self._solid)

    def parse(self):
        """
        """

        # Check if ascii or binary
        if self._inputFile.read(5) == "solid":
            print("ascii file")
            self._parseAscii()
        else:
            print("binary file")
            self._parseBinary()

        newSolid = Solid("%s_mapped" % self._solid.name)

        for i, facet in enumerate(self._solid.facets):
            newVertices = []
            for vertex in facet.vertices:
                newVertex = mapToPlane(vertex)
                newVertices.append(newVertex)

            newFacet = Facet(newVertices)  # normal is automatically computed by Facet object
            newSolid.addFacet(newFacet)

        self._outputFile.write(str(newSolid))

    def _parseAscii(self):
        """
        """
        self._inputFile.seek(0)
        inputStr = self._inputFile.read()

        # Iter over 'solid' (should be single)
        for solidStr in re.findall(r"solid\s(.*?)endsolid", inputStr, re.S):
            solidName = re.match(r"^(.*)$", solidStr, re.M).group(0)
            print("Processing object '%s'..." % solidName)

            self._solid.name = solidName

            # Iter over 'facet normal'
            for facetNormalStr in re.findall(r"facet\snormal\s(.*?)endfacet", solidStr, re.S):
                coords = [float(coordStr) for coordStr in facetNormalStr.split('\n')[0].split()]
                normal = Vector(coords)

                # Iter over 'outer loop' (should be only one)
                for outerLoopStr in re.findall(r"outer\sloop(.*?)endloop", facetNormalStr, re.S):
                    vertices = []

                    # Iter over 'vertex'
                    for vertexStr in re.findall(r"vertex\s(.*)$", outerLoopStr, re.M):
                        coords = [float(coordStr) for coordStr in vertexStr.split()]
                        vertex = Vertex(coords)
                        vertices.append(vertex)

                facet = Facet(vertices, normal)
                self._solid.addFacet(facet)

        self._inputFile.close()

    def _parseBinary(self):
        """
        """

        # Skip header
        self._inputFile.seek(80)

        nbFacets = struct.unpack("<I", self._inputFile.read(4))[0]
        print("found %d facets" % nbFacets)

        # Iterate over facets
        for i in range(nbFacets):

            # Get normal
            coords = struct.unpack("<fff", self._inputFile.read(3*4))
            normal = Vector(*coords)

            vertices = []

            # Iterate over vertices
            for j in range(3):
                coords = struct.unpack("<fff", self._inputFile.read(3*4))
                vertex = Vertex(coords)
                vertices.append(vertex)

            facet = Facet(vertices, normal)

            self._solid.addFacet(facet)

            # Skip byte count
            self._inputFile.seek(2, 1)

        self._inputFile.close()
